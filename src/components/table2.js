import * as React from 'react';
import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell , { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useState, useEffect } from 'react';
import axios from 'axios';
import TablePaginationActions from './panigator/panigator';
import { Button } from '@mui/material';
import TableHead from '@mui/material/TableHead';
import { styled } from '@mui/material/styles';

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));
export default function CustomPaginationActionsTable() {
    const [users, setUsers] = useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(3);

    // Avoid a layout jump when reaching the last page with empty rows.
    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - users.length) : 0;

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    //lấy dữ liệu users
        useEffect(() => {
            const  CALL_API_USER = async () => {
                 await axios(
                    {
                        method:"get",
                        url:"https://jsonplaceholder.typicode.com/users"
                    }
                )
                .then(res => {
                    console.log(res.data)
                    setUsers(res.data)
                })
                .catch(err => {
                    console.log(err)
                })
            }
            CALL_API_USER();
        }, [])
    return (
        <TableContainer component={Paper}>
        <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
            <TableHead style={{ backgroundColor: "black"}}>
                <StyledTableRow>
                    <StyledTableCell align='center'>ID</StyledTableCell>
                    <StyledTableCell align='center'>Fullname</StyledTableCell>
                    <StyledTableCell align='center'>Username</StyledTableCell>
                    <StyledTableCell align='center'>Phone Number</StyledTableCell>
                    <StyledTableCell align='center'>Email</StyledTableCell>
                    <StyledTableCell align='center'>Website</StyledTableCell>
                    <StyledTableCell align='center'>
                        Action
                    </StyledTableCell>
                </StyledTableRow>
            </TableHead>
            <TableBody>
            {(rowsPerPage > 0
                ? users.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : users
            ).map((row) => (
                <StyledTableRow key={row.name}>
                    <StyledTableCell align='center'>{row.id}</StyledTableCell>
                    <StyledTableCell align='center'>{row.name}</StyledTableCell>
                    <StyledTableCell align='center'>{row.username}</StyledTableCell>
                    <StyledTableCell align='center'>{row.phone}</StyledTableCell>
                    <StyledTableCell align='center'>{row.email}</StyledTableCell>
                    <StyledTableCell align='center'>{row.website}</StyledTableCell>
                    <TableCell align='center'>
                        <Button variant='contained' color="success">Chi tiết</Button>
                    </TableCell>
                </StyledTableRow>
            ))
            }

            {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
                </TableRow>
            )}
            </TableBody>
            <TableFooter>
            <TableRow>
                <TablePagination
                rowsPerPageOptions={[3, 5, 10, 25, { label: 'All', value: -1 }]}
                colSpan={3}
                count={users.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                    inputProps: {
                    'aria-label': 'rows per page',
                    },
                    native: true,
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
                />
            </TableRow>
            </TableFooter>
        </Table>
        </TableContainer>
    );
}